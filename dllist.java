// $Id: dllist.java,v 1.4 2013-09-04 10:02:24-07 - - $

class dllist {

    public enum position {FIRST, PREVIOUS, FOLLOWING, LAST};

    private class node {
	String item;
	node prev;
	node next;
    }

    private node first = null;
    private node current = null;
    private node last = null;
    private int currentposition = 0;

    // Counts the total number of elements in the linked list
    private int nodecount(){
	int i = 0;
	for (node one = first; one != null ; i++ ){
	    //System.out.println(one.item);
	    one = one.next;
	}
	return i;   
    }
    
    // Prints the current elements of the linked list in order
    private void printlist(){
	int i = 0;
	String print = "";
	for (node one = first; one != null; i++){
	    print += i + ":" + one.item + "\n";
	    one = one.next;
	}
	System.out.println(print);
    }

    public void setposition (position pos) {
	System.out.println("setposition ( position pos)");
	// currentposition = pos;
	//      throw new UnsupportedOperationException();
    }

    //Checks if the node is empty by checking nullility of first node
    public boolean isempty () {
	System.out.println( "isempty ( no args)");
	return (first == null) ? true : false;
	// throw new UnsupportedOperationException();
    }

    // Returns the item string stored in the current node
    public String getitem () {
	System.out.println("getitem ( no args)");
	return current.item;
	//throw new UnsupportedOperationException();
    }

    public int getposition () {
	System.out.println("getposition ( no args)");
	return currentposition;
	//throw new UnsupportedOperationException();
    }

    public void delete () {
	System.out.println("delete ( no args)");
       
	//throw new UnsupportedOperationException();
    }
   
    // This method inserts. You will always be inserting new elements.
    // A new node is created and its item is assigned the passed String. 
    // At the end of the function the current node becomes the new node.
    // 1) Check if the list is empty 
    //     - then initialize private vars to new node
    // 2) Case 1) insert at First position
    //     - assign prev pointer of first node to new node
    //     - assign next pointer of new node to first node
    //     - assign first node as new node
    // 3) Case 2) insert before current
    //     - if prev pointer is null then assign first node as new node
    //     - else assign next pointer of prev node to new node
    //     - and assign prev pointer of new node to prev node
    //     - assign prev pointer as new node
    //     - assign next pointer of new node to current node
    // 3) Case 3) insert after current
    //     - Reverse next/prev in Case 2 and change first --> last
    //     - Increment current position by 1
    // 4) Case 4) insert at Last position
    //     - reverse next/prev in Case 2 and change first --> last
    //     - change currentposition to total amount of items in list
    public void insert (String item, position pos) {
	System.out.println("insert (String item, position pos)");
	node one = new node();
	one.item = item;      
	if(isempty()){
	    first = one;
	    last = one;
	    current = one;
	    currentposition = 1;
	} else { 
	    switch (pos){
	    case FIRST:
		first.prev = one;
		one.next = first;
		first = one;
		currentposition = 1;
		break;
	    case PREVIOUS:
		if (current.prev == null){
		    first = one;
		} else {
		    current.prev.next = one;
		    one.prev = current.prev;
		}
		current.prev = one;
		one.next = current;
		break;
	    case FOLLOWING:
		if (current.next == null){
		    last = one;
		} else {
		    current.next.prev = one;
		    one.next = current.next;
		}
		current.next = one;
		one.prev = current;
		currentposition++;
		break;
	    case LAST:
		last.next = one;
		one.prev = last;
		last = one;
		currentposition = nodecount();
		break;
	    }
	    current = one;
	}
	System.out.println("Position: " +  getposition() + " Node count: " + nodecount());
	printlist();
	//throw new UnsupportedOperationException();
    }

}

