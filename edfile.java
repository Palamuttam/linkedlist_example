// $Id: edfile.java,v 1.53 2013-09-04 09:31:44-07 - - $
import static java.lang.System.*;
import java.io.*;
import java.util.Scanner;
class edfile{
    static void command (String command)
    {
	linetype(command.charAt(0));
	String out = "";
	if(command.equals("#")){
	    out = "anything";
	} else if (command.equals("$")){
	   out = "Current line is last line. Print new current line";
	} else if ( command.equals("*")){
	    out = "All of the lines in list are printed. Current line becomes last";
	} else if (command.equals(".")){
	    out = "The current line is printed";
	} else if (command.equals("0")){
	    out = "Current line is set to first line, and printed";
	} else if (command.equals("<")){
	    out = "Current line is set to previosu line";
	} else if (command.equals(">")){
	    out = "Current line is set to following line";
	} else if (command.equals("a")){
	    out = "inputline: text after a is inserted after current line";
	} else if (command.equals("d")){
	    out = "current line in the list is deleted";
	} else if (command.equals("i")){
	    out = "inputline: text after i is inserted before current line";
	} else if (command.equals("r")){
	    out = "filename: contents of file and read & insrted after current line";
	} else if (command.equals("w")){
	   out = "filename: all of the lines in the file are written to the specified file";
	} else {
	    out = "invalid character";
	}
	System.out.println(command + ": " + out);
    }

    static void readfile (String filename){
	File file = new File(filename);
	try {
	    Scanner scan = new Scanner(file);
	    while (scan.hasNextLine()){
		String line = scan.nextLine();
		System.out.println(line);
	    }
	} catch (FileNotFoundException e) {
	    System.out.println("Invalid file name");
	}

    }

    static void linetype (char firstchar){
	String stub;
	switch (firstchar){
	case '#':
	    stub = "comment line";
	    break;
	case '$' : case '*' : case '.' : 
	case '0' : case '<' : case '>' :
	case 'd' : case 'r' : case 'w' :
	    stub = "current line";
	    break;
	case 'i' :
	    stub = "input line";
	    break;
	default : 
	    stub = "invalid char";
	}
	System.out.println(stub);
    }

    public static void main (String[] args) {
	dllist lines = new dllist ();
	// test code for dllist class
	// lines.setposition(dllist.position.FIRST);
	// lines.isempty();
	
	// lines.getposition();
	// lines.delete();
	lines.insert("one",dllist.position.PREVIOUS);
	lines.insert("two",dllist.position.FOLLOWING);
	lines.insert("three",dllist.position.FOLLOWING);
	lines.insert("four",dllist.position.PREVIOUS);
	lines.insert("five",dllist.position.FOLLOWING);
	lines.insert("six",dllist.position.FIRST);
	lines.insert("seven",dllist.position.FIRST);
	lines.insert("eight",dllist.position.LAST);
	System.out.println(lines.getitem());
	// Scanner input = new Scanner(System.in);
	// for(int file = 0; file < args.length; file++){
	//     System.out.println(args[file]);
	//     ureadfile(args[file]);    
	// }
	// while(true){
	//     out.printf ("%s: ", misclib.program_name());
	//     String command = input.nextLine();
	//     command (command);
	// }
    }
    
}	       
